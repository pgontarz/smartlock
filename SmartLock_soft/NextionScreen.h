#pragma once
#include <Arduino.h>

class NextionScreen
{
private:
    //int state;
public:
    NextionScreen(/* args */);
    ~NextionScreen();

    void sendMainScreen();
    void sendAccesGranted();
    void sendAccesDenied();
    void sendRootLogin();
    void sendRootScreen();
};
