#include "NextionScreen.h"
#include "FPS_GT511C3.h"
#include "SoftwareSerial.h"

#define SerialRx 10
#define SerialTx 11
#define RelayPin 3
#define BuzzerPin 5

#define ACCES_DELAY 1200

/*
 * PINS FingerSensor:
 * LEFT: VCC, GND, TX,RX
 * 
 */

FPS_GT511C3 fps(SerialRx,SerialTx); 
NextionScreen nextionScreen;

void buzzerAcces()
{
  digitalWrite(BuzzerPin,HIGH);
  delay(100);
  digitalWrite(BuzzerPin,LOW);
  delay(100);
  digitalWrite(BuzzerPin,HIGH);
  delay(100);
  digitalWrite(BuzzerPin,LOW);
  delay(100);
}
void setup()
{
	Serial.begin(9600); 
  Serial2.begin(9600); 
  pinMode(RelayPin,OUTPUT);
  pinMode(BuzzerPin,OUTPUT);
  digitalWrite(RelayPin,HIGH);
	delay(100);
	fps.Open();    
	fps.SetLED(true);   

  //start buzzer sequence
  buzzerAcces();
}

void loop()
{
	// Identify fingerprint test
	if (fps.IsPressFinger())
	{
		fps.CaptureFinger(false);
		int id = fps.Identify1_N();
		
		if (id <200) 
		{

			//Serial.print("Verified ID:");
			//Serial.println(id);
			nextionScreen.sendAccesGranted();
			digitalWrite(RelayPin,LOW);
			delay(100);
			digitalWrite(RelayPin,HIGH);
      buzzerAcces();
      delay(ACCES_DELAY-100-400);
		}
		else
		{//if unable to recognize
			//Serial.println("Finger not found");
			nextionScreen.sendAccesDenied();
      digitalWrite(BuzzerPin,HIGH);
      delay(ACCES_DELAY);
      digitalWrite(BuzzerPin,LOW);
		}
	}
	else
	{
		//Serial.println("Please press finger");
	}
	delay(100);
}
